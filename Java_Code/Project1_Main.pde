import ddf.minim.*;
import ddf.minim.ugens.*;


String STUDENT_ID = "20170638";

// CONSTANTS
public final int OBSTACLE_WIDTH = 60;
public final int OBSTACLE_HEIGHT = 60;
public final int OBSTACLE_Y = 510;
public final int BALL_SIZE = 80;
public final int BALL_X = 200;
public final color BALL_COLOR = color (189, 131, 206);
public final float DEFAULT_SPEED = 10;
public final float SQUARE_SPEED = 3;
public final float TRIANGLE_SPEED = 2;
public final float CIRCLE_SPEED = 1;




class Game {
    private PImage imgs[];
    private ArrayList <Obstacles> obstacles;
    private ArrayList <Background> backgrounds;
    private int score;
    private int highestScore;
    private int lives;
    private float speed;
    private boolean start;



    Game () {
        imgs= new PImage [2];
	    imgs[0]= loadImage ("Heart-01.png");
	    imgs[1]= loadImage ("Heart-02.png");

        obstacles = new ArrayList <Obstacles> ();
        backgrounds = new ArrayList <Background> ();

        Background background1 = new Background (640);
        Background background2 = new Background (1920);
        backgrounds.add (background1);
        backgrounds.add (background2);

        int randomNumber = round (random (1.5, 4.5));
        switch (randomNumber) {
            case 2:
                obstacles.add (new Circle (1280 + OBSTACLE_WIDTH/2, OBSTACLE_WIDTH));
            case 3:
                obstacles.add (new Triangle (1280 + OBSTACLE_WIDTH/2, OBSTACLE_WIDTH, OBSTACLE_WIDTH));
            case 4:
                obstacles.add (new Square (1280 + OBSTACLE_WIDTH/2, OBSTACLE_WIDTH, OBSTACLE_HEIGHT));
        }

        score = 0;
        highestScore = 0;
        lives = 5;
        speed = DEFAULT_SPEED;
        start = true;
    }


    void draw () {
        // background graphics
        for (Background background : backgrounds) {
            background.move (speed);
            if (background.getX () < - 640) {
                background.moveTo (1920);
            }
            background.draw ();
        }

        // game start
        if (start) {
            player.play ();
            // obstacles
            int obsNum = obstacles.size ();
            if (obstacles.get (obsNum - 1).getX () < 1150) {
                // int randomNumber = round (random (0.5, 4.5));
                int xPosition = 1280 + OBSTACLE_WIDTH;
                int randomIndex = round (random (0.5, 4.5));
                if (randomIndex == 1) {
                    for (int i = 0; i < 2; i++) {
                        int position = 1280 + (2*i + 1)*OBSTACLE_WIDTH/2;
                        obstacles.add (randomObstacleGenerator (1.5, 4.5, position));
                    }
                } else {
                    obstacles.add (randomObstacleGenerator (1.5, 4.5, xPosition));
                }
            }

            // update obstacles
            for (int i = 0; i < obstacles.size (); i++) {
                Obstacles obstacle = obstacles.get (i);
                obstacle.move (speed);
                obstacle.draw ();
                
                if (ball.detectCollision (obstacle)) {
                    lives -= 1;
                    obstacles.remove (i);
                    speed += obstacle.changeSpeed ();
                    // println ("collided :" + obstacle.changeSpeed () + ", lives: " + lives);
                } else score += 1;

                if (obstacle.getX () < 0) obstacles.remove (i);
            }
        }

        // text score
        fill (0);
        PFont font = loadFont("GentiumBasic-Bold-48.vlw");
        textFont (font, 24);
        text ("Score: " + score, 1120, 60);

        // hearts
        imageMode (CENTER);
        for (int i = 0; i < lives; i++) {
            int x = 50 * (i+1);
            int y = 50;
            image (imgs[1], x, y, 50, 50);
        }

        // end game
        if (lives == 0) {
            // end game
            start = false;
            speed = 0;
            player.pause ();

            // save highest score
            if (highestScore < score) {
                highestScore = score;
            }

            // game over background
            rectMode (CENTER);
            fill (BACKGROUND_COLOR);
            noStroke ();
            rect (640, 360, 1280, 720);

            fill (FOREGROUND_COLOR);
            noStroke ();
            rect (640, 720, 1280, 360);

            // show 'game over' text
            fill (0);
            textAlign (CENTER);
            textFont (font, 48);
            text ("Game Over", 640, 280);

            // show 'final score' and 'highest score'
            textFont (font, 24);
            text ("Final Score: " + score, 640, 320);
            text ("Press Spacebar to restart.", 640, 400);
            text ("( Highest Score: " + highestScore + " )", 640, 350);
        }
    }

    void resetGame () {
        obstacles = new ArrayList <Obstacles> ();
        backgrounds = new ArrayList <Background> ();

        Background background1 = new Background (640);
        Background background2 = new Background (1920);
        backgrounds.add (background1);
        backgrounds.add (background2);

        obstacles.add (randomObstacleGenerator (1.5, 4.5, 1280 + OBSTACLE_WIDTH/2));

        score = 0;
        lives = 5;
        speed = DEFAULT_SPEED;
        start = true;
        player.rewind ();
    }
}

class JumpButton {
    private int x, y;
	private boolean pressState;

    JumpButton (int x, int y) {
        this.x = x;
        this.y = y;
		pressState = false;
    }

    void draw () {
        rectMode (CENTER);
        if (pressState == false) {
			fill (BUTTON_COLOR_OFF);
		} else fill (BUTTON_COLOR_ON);
        noStroke ();
        rect (x, y, BUTTON_WIDTH, BUTTON_HEIGHT);

        fill (255);
        PFont font = loadFont("GentiumBasic-Bold-48.vlw");
        textAlign (CENTER);
        textFont (font, 24);
        text ("Jump", x, y + 6);
    }

	void toggle (boolean pressState) {
		this.pressState = pressState;
	}

    boolean isOver () {
        if (mouseX < x - BUTTON_WIDTH/2 || mouseX > x + BUTTON_WIDTH/2) return false;
        if (mouseY < y - BUTTON_HEIGHT/2 || mouseY > y + BUTTON_HEIGHT/2) return false;
        return true;
    }

    boolean isPressed () {
        return pressState;
    }
}

// background objects: clouds
class Background {
    private int x;
    private PImage img;

    Background (int x) {
        this.x = x;
        img = loadImage ("Background.png");
    }

    void move (float speed) {
        x -= speed;
    }

    void draw () {
        image (img, x, 144, 1160, 288);
    }

    void moveTo (int position) {
        x = position;
    }

    int getX () {
        return x;
    }
}

class Obstacles {
    protected int x;

    Obstacles (int x) {
        this.x = x;
    }

    // blank method for overriding
    void draw () {
    }

    void move (float speed) {
        x -= speed;
    }

    float changeSpeed () {
        return 0;
    }

    int getX () {
        return x;
    }
}

class Square extends Obstacles {
    private int w, h;

    Square (int x, int w, int h) {
        super (x);
        this.w = w;
        this.h = h;
    }

    void draw () {
        rectMode (CENTER);
        fill (181, 234, 176);
        noStroke ();

        rect (x, OBSTACLE_Y, w, h);
    }

    float changeSpeed () {
        return SQUARE_SPEED;
    }
}

class Triangle extends Obstacles {
    private int w, h;

    Triangle (int x, int w, int h) {
        super (x);
        this.w = w;
        this.h = h;
    }

    void draw () {
        fill (234, 229, 176);
        noStroke ();

        triangle (x - w/2, OBSTACLE_Y + h/2, x + w/2, OBSTACLE_Y + h/2, x, OBSTACLE_Y - h/2);
    }

    float changeSpeed () {
        return TRIANGLE_SPEED;
    }
}

class Circle extends Obstacles {
    private int diam;

    Circle (int x, int diam) {
        super (x);
        this.diam = diam;
    }
    
    void draw () {
        ellipseMode (CENTER);
        fill (176, 234, 229);
        noStroke ();

        ellipse (x, OBSTACLE_Y, diam, diam);
    }

    float changeSpeed () {
        return CIRCLE_SPEED;
    }
}

// generate random obstacles
Obstacles randomObstacleGenerator (float min, float max, int x) {
    Obstacles obstacle = null;
    int randomNumber = round (random (min, max));
    switch (randomNumber) {
        case 2:
            obstacle = new Circle (x, OBSTACLE_WIDTH);
            break;
        case 3:
            obstacle = new Triangle (x, OBSTACLE_WIDTH, OBSTACLE_HEIGHT);
            break;
        case 4:
            obstacle = new Square (x, OBSTACLE_WIDTH, OBSTACLE_HEIGHT);
            break;
    }
    return obstacle;
}